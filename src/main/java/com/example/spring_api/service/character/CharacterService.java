package com.example.spring_api.service.character;

import com.example.spring_api.model.Character;
import com.example.spring_api.service.CrudService;

public interface CharacterService extends CrudService<Character, Integer> {

}
