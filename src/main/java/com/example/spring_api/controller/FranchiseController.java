package com.example.spring_api.controller;

import com.example.spring_api.mappers.CharacterMapper;
import com.example.spring_api.mappers.FranchiseMapper;
import com.example.spring_api.mappers.MovieMapper;
import com.example.spring_api.model.Character;
import com.example.spring_api.model.Franchise;
import com.example.spring_api.model.Movie;
import com.example.spring_api.model.dto.character.CharacterDTO;
import com.example.spring_api.model.dto.franchiseDTO.FranchiseDTO;
import com.example.spring_api.model.dto.franchiseDTO.UpdateFranchiseMovieListDTO;
import com.example.spring_api.model.dto.movie.MovieDTO;
import com.example.spring_api.service.character.CharacterService;
import com.example.spring_api.service.franchise.FranchiseService;
import com.example.spring_api.service.movie.MovieService;
import com.example.spring_api.utils.ApiErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping(path = "api/v1/franchise")
@Tag(name="Franchise Endpoints")
public class FranchiseController {
    private final FranchiseService franchiseService;
    private final MovieService movieService;
    private final CharacterMapper characterMapper;
    private final FranchiseMapper franchiseMapper;
    private final MovieMapper movieMapper;

    public FranchiseController(FranchiseService franchiseService, CharacterMapper characterMapper, MovieMapper movieMapper, MovieService movieService, FranchiseMapper franchiseMapper){
        this.franchiseService = franchiseService;
        this.characterMapper = characterMapper;
        this.movieMapper = movieMapper;
        this.movieService = movieService;
        this.franchiseMapper = franchiseMapper;
    }

    @Operation(summary = "Get a movie franchise by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseDTO.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "No movie with that ID exists in franchise",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @GetMapping("/{id}")  //GET: localhost:8080/api/v1/franchise/{ID}
    public ResponseEntity findById(@PathVariable int id){
        FranchiseDTO franchise = franchiseMapper.franchiseToFranchiseDTO(
                franchiseService.findById(id)
        );
        return ResponseEntity.ok(franchise);
    }

    @Operation(summary = "Get a character by franchise ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = FranchiseDTO.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "No character with that ID exists",
                    content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @GetMapping("/{id}/characters") //GET: localhost:8080/api/v1/franchise/{ID}/characters
    public ResponseEntity findCharacterWithIdInMovie(@PathVariable int id){
        var moviesFromFranchise = franchiseService.findById(id).getMovies();
        //Creating a new set so we can merge multiply sets into one.
        Set<Character> collection = new HashSet<>();

        //Loop through the movies from franchise and add all the characters into a single set.
        for (Movie movie : moviesFromFranchise) {
            collection.addAll(movie.getCharacters());
        }

        //Convert the characters into characterDTOs
        Collection<CharacterDTO> characterFromFranchise = characterMapper.characterToCharacterDto(
                collection
        );
        return ResponseEntity.ok(characterFromFranchise);
    }

    @Operation(summary = "Get all movie franchises")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = FranchiseDTO.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "No franchise with that ID exists",
                    content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @GetMapping //GET: localhost:8080/api/v1/franchise
    public ResponseEntity findAll(){
        Collection<FranchiseDTO> franchise = franchiseMapper.franchiseToFranchiseDTO(
                franchiseService.findAll()
        );
        return ResponseEntity.ok(franchise);
    }


    @Operation(summary = "Add a new franchise")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    description = "New franchise added",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseDTO.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Failed to add franchise",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @PostMapping //POST: localhost:8080/api/v1/franchise
    public ResponseEntity add(@RequestBody FranchiseDTO franchiseDTO){
        Franchise fran = franchiseService.add(franchiseMapper.franchiseDtoToFranchise(franchiseDTO));

        URI location = URI.create("franchise/" + fran.getId());
        return ResponseEntity.created(location).build();
    }


    @Operation(summary = "Update an existing franchise")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Franchise successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Your request cant be handled",
                    content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Franchise not found with supplied ID",
                    content = @Content)
    })
    @PutMapping("{id}") //PUT: localhost:8080/api/v1/franchise/{ID}
    public ResponseEntity update(@RequestBody FranchiseDTO franchiseDTO, @PathVariable int id){
        franchiseService.update(
                franchiseMapper.franchiseDtoToFranchise(franchiseDTO)
        );

        return ResponseEntity.noContent().build();
    }


    @Operation(summary = "Update the movie in a franchise")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Franchise in movie successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Your request cant be handled",
                    content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Franchise not found with supplied ID",
                    content = @Content)
    })
    @PutMapping("/{id}/updateMovie") //GET: localhost:8080/api/v1/franchise/{ID}/updateMovie
    public ResponseEntity updateMoviesInFranchise(@RequestBody UpdateFranchiseMovieListDTO updateFranchiseMovieListDTO, @PathVariable int id){
        Collection<MovieDTO> collection = new HashSet<>();

        var movieArray = updateFranchiseMovieListDTO.getMovies();

        var test = franchiseService.findById(id).getId();
        //Loop through the characters from array and add all the characters into a collection.
        for (int i = 0; i < movieArray.length; i++) {
            var movie = movieMapper.movieToMovieDto(movieService.findById(movieArray[i]));
            movie.setFranchise(test);
            collection.add(movie);
        }

        franchiseService.updateMoviesInFranchise(movieMapper.movieDtoToMovie(collection), id);

        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Delete an existing franchise")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Franchise successfully deleted",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Your request cant be handled",
                    content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Franchise not found with supplied ID",
                    content = @Content)
    })
    @DeleteMapping("{id}") //DELETE: localhost:8080/api/v1/franchise/{ID}
    public ResponseEntity delete(@PathVariable int id){
        Set<Movie> movies = franchiseService.findById(id).getMovies();

        movies.forEach(movie -> {
                    movie.setFranchise(null);
        });

        franchiseService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
