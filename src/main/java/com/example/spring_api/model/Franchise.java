package com.example.spring_api.model;

import com.fasterxml.jackson.annotation.JsonGetter;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Getter
@Setter
public class Franchise {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToMany(mappedBy = "franchise")
    private Set<Movie> movies;

    @Column(name = "franchise_fullName", length = 40, nullable = false)
    private String name;
    @Column(name = "franchise_desc", length = 200, nullable = false)
    private String description;

}
