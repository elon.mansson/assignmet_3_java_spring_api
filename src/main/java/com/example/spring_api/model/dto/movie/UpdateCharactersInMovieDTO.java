package com.example.spring_api.model.dto.movie;

import lombok.Data;

@Data
public class UpdateCharactersInMovieDTO {
    private int[] characters;
}
